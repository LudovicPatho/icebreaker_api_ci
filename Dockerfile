FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN pip install flask && pip install pytest-flask

COPY ./app /app
COPY ./test /test

CMD ["python", "/app/main.py"]