from flask import Response, url_for, jsonify
from app.main import create_app
import pytest 

"""
    Unit tests for flask
"""

def test_ping(client):
    res = client.get('/api/ping')
    assert res.json == {'ping': 'pong'}
    assert res.status_code == 200

def test_api(client):
    res = client.get('/api')
    assert res.status_code == 200
    assert isinstance(res.json['text'], str)
    assert len(res.json['text']) > 10

def test_api_int(client):
    test = 5,10,15
    for i in test:
        res = client.get(f"/api/{i}")
        assert res.status_code == 200
        assert len(res.json['data']) == i

def test_wrong_arg(client):
    res = client.get('/api/salut')
    assert res.status_code == 200
    assert res.json == {"Error" : "Must be an integer"}

@pytest.fixture
def app():
    app = create_app()
    return app
